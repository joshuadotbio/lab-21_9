package oop;

public class DecrementableCounter extends Counter {
	
	private int newCount;
	
	public void increment() {
		newCount++;
	}
	
	public void decrement() {
		if (newCount > 0) {
			newCount--;
		}
	}
	
	public int value() {
		return newCount;
	}
	
	public void reset() {
		newCount = 0;
	}
}
