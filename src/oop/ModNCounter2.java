package oop;

public class ModNCounter2 extends Counter {
	private int maxValue;
	
	public ModNCounter2(int n) {
		maxValue = n;
	}
	
	public int value() {
		if (super.value() > maxValue) {
			return super.value() % maxValue;
		} else {
			return super.value();
		}
	}
}
